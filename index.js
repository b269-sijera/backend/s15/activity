let firstName = "John Lenkelt";
console.log("First Name: " + firstName);

let lastName = "Sijera";
console.log("Last Name: " + lastName);

let myAge = 19;
console.log("Age: " + myAge)

let hobbies = 'Hobbies:'
console.log(hobbies)

let myHobbies = ['Coding', 'Traveling', 'Workout'];
console.log(myHobbies);

let workAddress = 'Work Address:';
console.log(workAddress);

let myWorkAddress = {
	houseNumber: '2003',
	street: 'Portofino',
	city: 'Metropolitan',
	state: 'Italy'
}
console.log(myWorkAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let age = 40;
console.log("My current age is: " + age);
	
let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {

	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false
}
	
console.log("My Full Profile: ")
	console.log(profile);

	{
		let fullName = "Bucky Barnes";
		console.log("My bestfriend is: " + fullName);
	}
	
const lastLocation = "Arctic Ocean";
	{
		let lastLocation = "Atlantic Ocean";
	}
	console.log("I was found frozen in: " + lastLocation);

